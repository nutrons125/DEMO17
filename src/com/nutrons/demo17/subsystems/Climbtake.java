package com.nutrons.demo17.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.nutrons.demo17.RobotMap;
import edu.wpi.first.wpilibj.command.Subsystem;

public class Climbtake extends Subsystem {

    //Speed Controllers, these are the devices that control the power of the motors.
    private TalonSRX climbtakeMain = new TalonSRX(RobotMap.CLIMBTAKE_MOTOR_1);
    private TalonSRX climbtakeFollower = new TalonSRX(RobotMap.CLIMBTAKE_MOTOR_2);

    private static final double CLIMBTAKE_HI_POWER = 1.0;
    private static final double CLIMBTAKE_LOW_POWER = -CLIMBTAKE_HI_POWER;
    private static final double CLIMBTAKE_PO = 1.0;

    public Climbtake() {
        //Follower Controllers setup, telling the followers to mimic their leaders.
        this.climbtakeFollower.follow(climbtakeMain);

        //Configuring the minimum power and maximum power that can be supplied to each motor. Just a bounds thing.
        this.climbtakeMain.configPeakOutputForward(CLIMBTAKE_HI_POWER, 0);
        this.climbtakeMain.configPeakOutputReverse(CLIMBTAKE_LOW_POWER, 0);
        this.climbtakeMain.configNominalOutputForward(0.0, 0);
        this.climbtakeMain.configNominalOutputReverse(0.0, 0);
        this.climbtakeFollower.configPeakOutputForward(CLIMBTAKE_HI_POWER, 0);
        this.climbtakeFollower.configPeakOutputReverse(CLIMBTAKE_LOW_POWER, 0);
        this.climbtakeFollower.configNominalOutputForward(0.0, 0);
        this.climbtakeFollower.configNominalOutputReverse(0.0, 0);

        //Setting one of the sides of the climbtake to be inverted or not could be important
        this.climbtakeMain.setInverted(false);
        this.climbtakeFollower.setInverted(false);
    }

    public void climbtake() {
        this.climbtakeMain.set(ControlMode.PercentOutput, CLIMBTAKE_PO);
    }

    public void stopClimbtake() {
        this.climbtakeMain.set(ControlMode.PercentOutput, 0.0);
    }

    @Override
    protected void initDefaultCommand() {
    }

}
