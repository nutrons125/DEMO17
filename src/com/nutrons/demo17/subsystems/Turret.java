package com.nutrons.demo17.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.nutrons.demo17.RobotMap;
import com.nutrons.demo17.commands.Turret.TurnHoodCmd;
import edu.wpi.first.wpilibj.command.Subsystem;

public class Turret extends Subsystem {

    //Speed Controllers, these are the devices that control the power of the motors.
    private TalonSRX hood = new TalonSRX(RobotMap.HOOD_MOTOR_A);

    private static final double HOOD_HI_POWER = 1.0;
    private static final double HOOD_LOW_POWER = -HOOD_HI_POWER;

    private static final double PVAL = 0.45;
    private static final double IVAL = 0.0;
    private static final double DVAL = 0.0;
    private static final double FVAL = 0.0;

    public Turret() {
        //Configuring the minimum power and maximum power that can be supplied to each motor. Just a bounds thing.
        this.hood.configPeakOutputForward(HOOD_HI_POWER, 0);
        this.hood.configPeakOutputReverse(HOOD_LOW_POWER, 0);
        this.hood.configNominalOutputForward(0.0, 0);
        this.hood.configNominalOutputReverse(0.0, 0);

        enableBreakMode();

        //This is here for quick inversion of the turret
        this.hood.setInverted(false);
    }

    //Enables break mode in neutral, where neutral is no power supplied.
    public void enableBreakMode() {
        this.hood.setNeutralMode(NeutralMode.Brake);
    }

    //Disables break mode in neutral, where neutral is no power supplied.
    public void disableBreakMode() {
        this.hood.setNeutralMode(NeutralMode.Coast);
    }

    public void turnHood(double pow) {
        this.hood.set(ControlMode.PercentOutput, pow);
    }

    @Override
    protected void initDefaultCommand() {
        setDefaultCommand(new TurnHoodCmd());
    }

}
