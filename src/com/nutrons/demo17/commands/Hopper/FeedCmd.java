package com.nutrons.demo17.commands.Hopper;

import com.nutrons.demo17.Robot;
import edu.wpi.first.wpilibj.command.Command;

public class FeedCmd extends Command {

    public FeedCmd() {
        requires(Robot.hopper);
    }

    protected void initialize() {

    }

    protected void execute() {
        Robot.hopper.feed();
        Robot.hopper.roll();
    }

    protected boolean isFinished() {
        return false;
    }

    protected void end() {
        Robot.hopper.stopFeed();
        Robot.hopper.stopRoll();
    }

    protected void interrupted() {
        end();
    }

}