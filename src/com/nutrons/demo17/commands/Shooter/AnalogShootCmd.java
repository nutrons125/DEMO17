package com.nutrons.demo17.commands.Shooter;

import com.nutrons.demo17.Robot;
import edu.wpi.first.wpilibj.command.Command;

public class AnalogShootCmd extends Command {

    public AnalogShootCmd() {
        requires(Robot.shooter);
    }

    protected void initialize() {

    }

    protected void execute() {
        Robot.shooter.shoot(Robot.oi.getOpRightTrigger());
    }

    protected boolean isFinished() {
        return false;
    }

    protected void end() {
    }

    protected void interrupted() {
        end();
    }

}