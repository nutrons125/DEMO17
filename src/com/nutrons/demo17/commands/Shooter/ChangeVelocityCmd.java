package com.nutrons.demo17.commands.Shooter;

import com.nutrons.demo17.Robot;
import edu.wpi.first.wpilibj.command.Command;
import com.nutrons.demo17.subsystems.Shooter.*;

public class ChangeVelocityCmd extends Command {

    public Velocities newVel;

    public ChangeVelocityCmd(Velocities newVel) {
        requires(Robot.shooter);
        this.newVel = newVel;
    }

    protected void initialize() {

    }

    protected void execute() {
        Robot.shooter.changeShooterVel(newVel);
    }

    protected boolean isFinished() {
        return false;
    }

    protected void end() {

    }

    protected void interrupted() {
        end();
    }

}