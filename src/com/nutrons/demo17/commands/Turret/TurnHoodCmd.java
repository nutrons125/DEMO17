package com.nutrons.demo17.commands.Turret;

import com.nutrons.demo17.Robot;
import edu.wpi.first.wpilibj.command.Command;

public class TurnHoodCmd extends Command {


    public TurnHoodCmd() {
        requires(Robot.turret);
    }

    protected void initialize() {

    }

    protected void execute() {
        Robot.turret.turnHood(Robot.oi.getOpLeftStickX());
    }

    protected boolean isFinished() {
        return false;
    }

    protected void end() {
    }

    protected void interrupted() {
        end();
    }

}